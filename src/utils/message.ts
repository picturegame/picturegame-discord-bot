import * as Discord from 'discord.js';
import * as winston from 'winston';

import { logError } from './error';
import { sleep } from './promise';

export const MessageCharacterLimit = 2000;
const cooldownWarningTimeout = 10000;

export async function deleteIfRejected(message: Discord.Message, rejection?: Discord.Message | Discord.Message[]) {
    if (!rejection || Array.isArray(rejection)) { return; }

    await sleep(cooldownWarningTimeout);

    try {
        await Promise.all([
            message.delete(),
            rejection.delete(),
        ]);
    } catch (e) {
        // Likely one of the messages was already deleted. Log a warning just in case but we probably don't care
        logError(e, 'Failed to delete rejection message', winston.warn);
    }
}

export function trySendDM(user: Discord.User, message: string | Discord.MessageOptions) {
    let promise: Promise<any>;
    // Force TypeScript to use the correct overload for the two types
    // TODO: Is there a better way? Does a newer version of TS make this unnecessary?
    if (typeof message === 'string') {
        promise = user.send(message);
    } else {
        promise = user.send(message);
    }
    return promise.catch(e => {
        winston.warn('Failed to send DM', {
            user: user.username,
            error: e,
        });
    });
}

export async function sendLongMessage(channel: Discord.TextChannel, message: string) {
    if (message.length <= MessageCharacterLimit) {
        await channel.send(message);
        return;
    }

    const lines = message.split('\n');
    let index = 0;
    let subMessage = '';
    while (index < lines.length) {
        const nextLine = lines[index++];
        // Assume that no single line is over 2000 characters long
        if (subMessage.length + 1 + nextLine.length <= MessageCharacterLimit) {
            subMessage += '\n' + nextLine;
        } else {
            await channel.send(subMessage);
            subMessage = nextLine;
        }
    }
    await channel.send(subMessage);
}
