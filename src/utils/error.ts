import * as winston from 'winston';

/**
 * Explicitly unpacks Error objects for logging
 * For some reason, just doing winston.error(message, err) results in an empty metadata object
 */
export function logError(
    err: any,
    message: string,
    logFn: winston.LeveledLogMethod = winston.error,
    skipDiscord: boolean = false) {

    const metadata = { ...err };
    if (err instanceof Error) {
        metadata.stack = err.stack;
        metadata.errorMessage = err.message;
        metadata.errorName = err.name;
    }
    if (skipDiscord) {
        metadata.skipDiscord = skipDiscord;
    }
    logFn(message, metadata);
}
