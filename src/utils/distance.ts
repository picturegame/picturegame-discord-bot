import * as Discord from 'discord.js';
import * as winston from 'winston';

import { Command } from '../model/bot';
import { State } from '../model/state';

import { getUserInfoFromMessage } from '../utils/user';

// https://en.wikipedia.org/wiki/Levenshtein_distance
function distance(a: string, b: string): number {
    let v0: number[] = [];
    let v1: number[] = [];

    for (let i = 0; i <= b.length; i++) {
        v0[i] = i;
    }

    for (let i = 0; i < a.length; i++) {
        v1[0] = i + 1;

        for (let j = 0; j < b.length; j++) {
            const subCost = a[i] === b[j] ? 0 : 1;
            v1[j + 1] = Math.min(v1[j] + 1, v0[j + 1] + 1, v0[j] + subCost);
        }

        const temp = v0;
        v0 = v1;
        v1 = temp;
    }

    return v0[b.length];
}

function bestDistance(value: string, commands: string[]): string | undefined {
    const lower = value.toLowerCase();

    let suggestion: string | undefined = undefined;
    let minDistance = 5; // Don't suggest things that are too different

    commands.some(name => {
        if (name.length === 1) {
            return false; // Don't suggest the one-letter aliases
        }
        const dist = distance(name, lower);
        if (dist < minDistance) {
            suggestion = name;
            minDistance = dist;
        }
        if (dist === 1) {
            // Can't get any better than this
            return true;
        }
        return false;
    });

    return suggestion;
}

function getCommandsWithPermission(author?: Discord.GuildMember | null): string[] {
    const roles = author?.roles.cache.map(r => r.name.toLowerCase());
    const state = State.getState();
    return state.data.commands.filter(c => {
        if (!roles) {
            return true;
        }
        const command = state.getCommand(c) as Command;
        return !command.permissions || roles.some(r => command.permissions!.has(r));
    });
}

export function commandNotFound(commandName: string, message: Discord.Message): string {
    const commands = getCommandsWithPermission(message.member);
    const suggestion = bestDistance(commandName, commands);

    winston.info('Command not found', {
        input: commandName,
        suggestion,
        ...getUserInfoFromMessage(message),
    });

    let response = `Unknown command: "${commandName.truncate()}"`;
    if (suggestion) {
        response += `, did you mean *${suggestion}*?`;
    }
    return response;
}
