import * as process from 'process';

import { initialize } from './commands';
import { setupReactionHandlers } from './reactions';
import * as setup from './setup';
import * as utils from './utils';

async function setupBot() {
    setup.setupLogging();
    await setup.setupDiscord();
    await setup.setupData();
    await utils.tryOrLog(setup.cleanupRoundSubscriptions(), 'Failed to cleanup round subscriptions');
    setup.setupSocket();
    setupReactionHandlers();
    await initialize();
}

setupBot().catch(e => {
    utils.logError(e, 'Error setting up');
    process.exit(1);
});
