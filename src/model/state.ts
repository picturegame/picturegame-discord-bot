import * as dgram from 'dgram';
import * as Discord from 'discord.js';
import * as fs from 'fs';
import * as path from 'path';
import * as picturegame from 'picturegame-api-wrapper';
import * as winston from 'winston';

import { tryOrLog } from '../utils/promise';
import { cleanName } from '../utils/sanitizer';

import * as Bot from './bot';
import { loadState } from './PersistableState';
import { ReactionKey } from './ReactionKey';

import * as config from '../../config.json';

const Intents = Discord.Intents.FLAGS;
const BotIntents =
    Intents.GUILDS
    | Intents.GUILD_MEMBERS
    | Intents.GUILD_PRESENCES
    | Intents.GUILD_MESSAGES
    | Intents.GUILD_MESSAGE_REACTIONS
    | Intents.DIRECT_MESSAGES
    | Intents.DIRECT_MESSAGE_REACTIONS;

export class State {
    private static _state: State = new State();

    private _discord: Discord.Client;
    private _socket: dgram.Socket;
    private _api: picturegame.PictureGame;

    private _commands: Map<string, Bot.Command>;
    private _data: Bot.DataState;

    private _allowedChannels: Set<string>;
    private _forbiddenChannels: Set<string>;

    public ignoreModmailAuthors: Set<string>;
    public ignoreModmailSubjects: Set<string>;

    constructor() {
        if (State._state) {
            winston.error('Attempted to re-instantiate singleton-state - abort');
        }

        this._discord = new Discord.Client({ ws: { intents: BotIntents } });

        this._socket = dgram.createSocket('udp4');
        this._api = new picturegame.PictureGame(config.api);

        this._data = {
            leaderboard: [],
            players: new Map<string, Bot.Player>(),
            round: {},
            commands: [],
            commandCooldowns: new Map<string, Bot.CooldownState>(),
            customCooldownResponses: new Map<string, string>(),
            recentStats: new Map<string, Bot.RecentCommands<Bot.Player>>(),
            reactableMessages: new Map<ReactionKey, Bot.ReactableMessage<any>>(),
            messageTypes: new Map<string, string>(),
            ...loadState(),
        };

        this._allowedChannels = new Set(config.bot.globalAllowedChannels?.map(c => c.toLowerCase()));
        this._forbiddenChannels = new Set(config.bot.globalForbiddenChannels?.map(c => c.toLowerCase()));

        this.ignoreModmailAuthors = new Set(config.bot.ignoreModmailAuthors?.map(c => c.toLowerCase()));
        this.ignoreModmailSubjects = new Set(config.bot.ignoreModmailSubjects?.map(c => c.toLowerCase()));

        State._state = this;
    }

    public static getState(): State {
        return State._state;
    }

    public async updateLeaderboard() {
        // Reset the cooldowns on all of the stats-related commands
        this.resetCooldowns('leaderboard', 'stats', 'rank');

        const apiLeaderboard = await tryOrLog(this.api.getLeaderboard(), 'Error updating leaderboard') ?? [];

        const players = new Map<string, Bot.Player>();
        const leaderboard: Bot.Player[] = apiLeaderboard.map(p => {
            const player: Bot.Player = {
                name: cleanName(p.username),
                rank: p.rank as number,
                winCount: p.roundList!.length,
            };
            players.set(player.name.toLowerCase(), player);
            return player;
        });

        this.data.players = players;
        this.data.leaderboard = leaderboard;

        winston.info('Leaderboard updated!');
    }

    public async reportStartup() {
        const versionFile = path.join(__dirname, '..', '..', 'VERSION');
        let version = '99.99.99-dev';
        if (fs.existsSync(versionFile)) {
            version = fs.readFileSync(versionFile, 'utf8');
        }

        await tryOrLog(this._api.setStatus({
            name: 'picturegame-discord-bot',
            startTime: Math.floor(Date.now() / 1000),
            version,
        }), 'Failed to report startup to API');
    }

    get api() {
        return this._api;
    }

    get discord() {
        return this._discord;
    }

    get data() {
        return this._data;
    }

    get socket() {
        return this._socket;
    }

    get allowedChannels() {
        return this._allowedChannels;
    }

    get forbiddenChannels() {
        return this._forbiddenChannels;
    }

    public getCommand(command: string) {
        return this._commands.get(command.toLowerCase());
    }

    public set commands(commands: Map<string, Bot.Command>) {
        this._commands = commands;
        this._data.commands = Array.from(commands.keys());
    }

    // Discord helpers
    public login = () => {
        this._discord.login(config.discord.auth.token);
    }

    get server() {
        const server = this._discord.guilds.cache.get(config.discord.serverId);
        if (!server) {
            throw new Error('Unable to get server');
        }
        return server;
    }

    get channel() {
        const channel = this.server.channels.cache.get(config.discord.channelId);
        if (!channel) {
            throw new Error('Unable to get channel');
        }
        return channel as Discord.TextChannel;
    }

    get userId() {
        return this._discord.user?.id;
    }

    // TODO: Move this somewhere more appropriate - this whole class needs a refactor
    announce(text: string) {
        return tryOrLog(this.channel.send(text), 'Failed to send message to channel');
    }

    getRole(roleName: string) {
        return this.server.roles.cache.find(r => r.name === roleName);
    }

    resetCooldowns(...commandNames: string[]) {
        commandNames.forEach(command => {
            this._data.commandCooldowns.delete(command);
            if (command === 'stats' || command === 'rank') {
                this._data.recentStats = new Map<string, Bot.RecentCommands<Bot.Player>>();
            }
        });
    }

    startCooldown(commandName: string, customResponse?: string) {
        const command = this._commands.get(commandName);
        if (!command?.cooldown) {
            return;
        }

        const cooldownTime = new Date();
        cooldownTime.setUTCMinutes(cooldownTime.getUTCMinutes() + command.cooldown);

        const cooldownState: Bot.CooldownState = {};
        this.server.channels.cache.forEach(channel => {
            cooldownState[channel.id] = cooldownTime;
        });
        this._data.commandCooldowns.set(command.command, cooldownState);

        if (customResponse) {
            this._data.customCooldownResponses.set(command.command, customResponse);
        }
    }
}
