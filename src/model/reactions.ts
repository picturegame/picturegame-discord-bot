import * as discord from 'discord.js';
import * as winston from 'winston';

import { tryOrLog, trySendDM } from '../utils';

import { ReactionHandler, SendMessageCallback } from './bot';
import { toReactionKey } from './ReactionKey';
import { State } from './state';

const reactionHandlers = new Map<string, ReactionHandler<any>>();

export function register(...handlers: ReactionHandler<any>[]) {
    handlers.forEach(handler => reactionHandlers.set(handler.type, handler));
}

export async function handleReaction(reaction: discord.MessageReaction, user: discord.User) {
    if (!reaction.me) { return; }

    const state = State.getState();

    if (state.discord.user?.id === user.id) { return; }

    const messageType = state.data.messageTypes.get(reaction.message.id);
    if (!messageType) { return; }

    const key = toReactionKey(reaction.message.channel, messageType);
    const message = state.data.reactableMessages.get(key);
    if (!message) { return; }

    const handler = getHandler(message.type);

    if (message.userId !== user.id) {
        const newMessageState = Object.assign({}, message.state);
        handler.update(newMessageState, reaction.emoji);
        await setupMessage({
            handler,
            messageType: message.type,
            messageState: newMessageState,
            onSend: (body: string | discord.MessageOptions) => trySendDM(user, body),
            userId: user.id,
        });
    } else {
        const shouldUpdate = handler.update(message.state, reaction.emoji);
        if (shouldUpdate) {
            await reaction.message.edit(handler.getContents(message.state));
        }
    }
}

interface SetupMessageArgs<T> {
    messageState: T;
    messageType: string;
    userId: string;
    handler: ReactionHandler<T>;
    onSend: SendMessageCallback;
}

async function setupMessage<T>(args: SetupMessageArgs<T>) {
    const { handler } = args;
    const message = await args.onSend(handler.getContents(args.messageState)) as discord.Message;

    if (!message) {
        winston.warn('Failed to setup reactable message. Additional logging likely precedes this message.', {
            messageType: args.messageType,
        });
        return;
    }

    const emojis = typeof handler.emojis === 'function' ? handler.emojis() : handler.emojis;
    for (const emoji of emojis) {
        await message.react(emoji);
    }

    const state = State.getState();
    const key = toReactionKey(message.channel, args.messageType);

    const oldMessage = state.data.reactableMessages.get(key);
    if (oldMessage) {
        state.data.messageTypes.delete(oldMessage.messageId);
        await tryOrLog((async () => {
            const original = await message.channel.messages.fetch(oldMessage.messageId);
            await clearReactions(original, state.userId);
        })(), 'Failed to clear reactions', winston.warn);
    }

    state.data.messageTypes.set(message.id, args.messageType);
    state.data.reactableMessages.set(key, {
        messageId: message.id,
        state: args.messageState,
        type: args.messageType,
        userId: args.userId,
    });
}

async function clearReactions(message?: discord.Message, userId?: string) {
    if (!message) { return; }

    if (message.channel.type !== 'dm') {
        message.reactions.removeAll();
        return;
    }

    // Need to delete one-by-one for a DM channel
    await Promise.all(message.reactions.cache.filter(r => r.me).map(r => r.users.remove(userId)));
}

export async function initializeReactableMessage(
    commander: discord.User,
    messageType: string,
    onSend: SendMessageCallback,
    ...args: any[]) {

    const handler = getHandler(messageType);
    const messageState = handler.init(...args);
    await setupMessage({
        handler,
        messageState,
        messageType,
        onSend,
        userId: commander.id,
    });
}

function getHandler<T>(messageType: string): ReactionHandler<T> {
    const handler = reactionHandlers.get(messageType);
    if (!handler) {
        throw new Error(`No reaction handler found for message type ${messageType}`);
    }
    return handler;
}
