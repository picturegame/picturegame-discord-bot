import { LogLevel } from '../utils/logging';

export enum RedditBotMessageType {
    CurrentRound = 'current-round',
    Comment = 'comment',
    CommentEdit = 'comment-edit',
    ModPost = 'mod-post',
    ModMail = 'mod-mail',
    RoundCorrection = 'round-correction',
    Log = 'log',
}

export enum RoundStatus {
    New = 'new',
    Solved = 'solved',
    Abandoned = 'abandoned',
    Deleted = 'deleted',
}

export interface RedditBotMessage {
    type: RedditBotMessageType;
}

export interface CurrentRoundMessage {
    type: RedditBotMessageType.CurrentRound;
    status: RoundStatus;
}

export function isRoundStatus(message: RedditBotMessage): message is CurrentRoundMessage {
    return message.type === RedditBotMessageType.CurrentRound;
}

export interface NewRoundMessage extends CurrentRoundMessage {
    status: RoundStatus.New;
    url: string;
    id: string;
    host: string;
    title: string;
    roundNumber: number;
    postTime: number;
}

export function isNewRound(message: CurrentRoundMessage): message is NewRoundMessage {
    return message.status === RoundStatus.New;
}

export interface AbandonedRoundMessage extends CurrentRoundMessage {
    status: RoundStatus.Abandoned | RoundStatus.Deleted;
}

export function isAbandonedRound(message: CurrentRoundMessage): message is AbandonedRoundMessage {
    return message.status === RoundStatus.Abandoned || message.status === RoundStatus.Deleted;
}

export interface SolvedRoundMessage extends CurrentRoundMessage {
    status: RoundStatus.Solved;
    winner: string;
    winCount: number;
    streak: number;
    gap: number;
    commentId: string;
    winTime: number;
}

export function isSolvedRound(message: CurrentRoundMessage): message is SolvedRoundMessage {
    return message.status === RoundStatus.Solved;
}

export interface CommentMessage {
    author: string;
    isOp: boolean;
    postId: string; // NOT prefixed with t3_
    commentId: string; // NOT prefixed with t1_
    parentId: string; // t3_submissionId for root, t1_commentId for non-root
    parentAuthor?: string; // Only specified if isOp is true
    body: string;
}

export interface NewCommentMessage extends CommentMessage {
    type: RedditBotMessageType.Comment;
}

export function isNewComment(message: RedditBotMessage): message is NewCommentMessage {
    return message.type === RedditBotMessageType.Comment;
}

export interface CommentEditMessage extends CommentMessage {
    type: RedditBotMessageType.CommentEdit;
    edited: number;
}

export function isCommentEdit(message: RedditBotMessage): message is CommentEditMessage {
    return message.type === RedditBotMessageType.CommentEdit;
}

export interface ModPostMessage {
    type: RedditBotMessageType.ModPost;
    author: string;
    title: string;
    postId: string;
}

export function isModPost(message: RedditBotMessage): message is ModPostMessage {
    return message.type === RedditBotMessageType.ModPost;
}

export interface ModMailMessage {
    type: RedditBotMessageType.ModMail;
    conversation: string;
    subject: string;
    author: string;
    body: string;
}

export function isModmail(message: RedditBotMessage): message is ModMailMessage {
    return message.type === RedditBotMessageType.ModMail;
}

export interface RoundCorrectionMessage {
    type: RedditBotMessageType.RoundCorrection;
    ordinal: 0 | 1;
    commentId: string; // The id of the target/new comment
    submissionId: string;
    winner: string; // The target/new winner
    roundNumber: number;
}

export interface PendingRoundCorrectionMessage extends RoundCorrectionMessage {
    ordinal: 0;
    correcter: string;
}

export interface CompletedRoundCorrectionMessage extends RoundCorrectionMessage {
    ordinal: 1;
    winTime: number;
}

export function isRoundCorrection(message: RedditBotMessage): message is RoundCorrectionMessage {
    return message.type === RedditBotMessageType.RoundCorrection;
}

export function isPendingRoundCorrection(message: RoundCorrectionMessage): message is PendingRoundCorrectionMessage {
    return message.ordinal === 0;
}

export interface LogMessage {
    type: RedditBotMessageType.Log;
    level: LogLevel;
    message: string;
    data?: any;
}

export function isLogMessage(message: RedditBotMessage): message is LogMessage {
    return message.type === RedditBotMessageType.Log;
}
