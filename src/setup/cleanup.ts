import * as winston from 'winston';

import { saveState } from '../model/PersistableState';
import { State } from '../model/state';
import { CurrentRoundId } from '../reddit/Subscription';
import { getArchiveThreshold } from '../utils';

export async function cleanupRoundSubscriptions() {
    const state = State.getState();
    const api = state.api;

    winston.info('Cleaning up round subscriptions');

    const allIds = new Set([
        ...state.data.roundSubscriptions_players.keys(),
        ...state.data.roundSubscriptions_host.keys(),
    ]);

    allIds.delete(CurrentRoundId);

    if (allIds.size === 0) {
        winston.info('No subscriptions to cleanup');
        return;
    }

    const idsToRemove: string[] = [];

    const threshold = getArchiveThreshold();

    while (true) {
        const idList = [...allIds].map(id => `"${id}"`).join(',');
        const response = await api.getRounds({
            filter: `id in [${idList}]`,
            select: ['postTime', 'id'],
        });

        for (const round of response.results) {
            allIds.delete(round.id!);
            if (round.postTime && round.postTime < threshold) {
                idsToRemove.push(round.id!);
            }
        }

        if (response.totalNumResults <= response.pageSize) {
            break;
        }
    }

    for (const id of idsToRemove) {
        state.data.roundSubscriptions_players.delete(id);
        state.data.roundSubscriptions_host.delete(id);
    }
    // Any ids left in this set have no corresponding round
    for (const id of allIds) {
        state.data.roundSubscriptions_players.delete(id);
        state.data.roundSubscriptions_host.delete(id);
    }

    const currentRound = state.data.round;
    if (currentRound.roundNumber) {
        for (const [roundNumber] of state.data.roundNotifications) {
            if (roundNumber < currentRound.roundNumber
                || roundNumber === currentRound.roundNumber && currentRound.winner) {
                state.data.roundNotifications.delete(roundNumber);
            }
        }
    }

    await saveState(state.data);

    winston.info('Successfully cleaned up round subscriptions', {
        numArchived: idsToRemove.length,
        numNotFound: allIds.size,
    });
}
