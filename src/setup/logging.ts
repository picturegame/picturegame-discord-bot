import * as winston from 'winston';
import * as Transport from 'winston-transport';

import * as config from '../../config.json';
import { logError } from '../utils/error';
import { writeLogToDiscord } from '../utils/logging';

const DiscordBotServiceName = 'picturegame-discord-bot';

export function setupLogging() {
    winston.configure({
        level: config.bot.log.level,
        exitOnError: false,
    });

    if (config.bot.log.console) {
        winston.add(new winston.transports.Console({
            format: winston.format.combine(
                winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
                winston.format.printf(info => {
                    const { timestamp, level, message, ...data } = info;
                    return `[${timestamp}] ${level}: ${message} ${JSON.stringify(data)}`;
                })),
        }));
    }

    winston.add(new DiscordTransport());
}

class DiscordTransport extends Transport {
    log(info: any, callback: () => void = () => { }) {
        const { level, message, ...data } = info;
        if (data.skipDiscord) {
            callback();
            return;
        }

        writeLogToDiscord(DiscordBotServiceName, level, message, data)
            .then(() => {
                this.emit('logged', info);
                callback();
            })
            .catch(e => {
                logError(e, 'Failed to write logs to discord', winston.warn, true);
                callback();
            });
    }
}
