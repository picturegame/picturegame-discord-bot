import * as Bot from '../model/bot';

import * as config from '../../config.json';

export const rulesCommands: Bot.CommandSpec = {
    description: 'Rules',
    commands: [
        {
            command: 'rules',
            description: `Gives a link to the /r/${config.reddit.subreddit} rules`,
            execute: () => async send => {
                await send(
                    `The complete rules of ${config.reddit.subreddit} can be found here:\n` +
                    `https://www.reddit.com/r/${config.reddit.subreddit}/wiki/rules\n\n` +
                    `For Discord server rules, visit <#${config.discord.rulesChannelId}>`);
            },
        },
    ],
};
