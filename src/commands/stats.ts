import * as Discord from 'discord.js';

import * as Bot from '../model/bot';
import { initializeReactableMessage } from '../model/reactions';
import { State } from '../model/state';

import * as utils from '../utils';

import * as config from '../../config.json';

export const statsCommands: Bot.CommandSpec = {
    description: 'Statistics',
    commands: [
        {
            command: 'stats',
            description: `Returns the /r/${config.reddit.subreddit} stats of the given user(s)`,
            parameters: ['...users'],
            minParameters: 0,
            execute: (message, ...params) => async send => {
                const state = State.getState();
                const leaderboard = state.data.leaderboard;
                const players = state.data.players;

                await executeStats({
                    message,
                    params,
                    send,
                    getPlayer: name => {
                        const clean = utils.cleanName(name).toLowerCase();
                        if (players.has(clean)) {
                            return players.get(clean);
                        }

                        const rank = parseInt(name, 10);
                        if (!isNaN(rank) && rank >= 1 && rank <= leaderboard.length) {
                            return leaderboard[rank - 1];
                        }
                    },
                    getNotFoundMessage: invalid => `Sorry, I couldn't find any wins for *${invalid.join('*, *')}*`,
                });
            },
        },
        {
            command: 'rank',
            description: `Returns the /r/${config.reddit.subreddit} stats for the person(s) at the given rank(s)`,
            parameters: ['...ranks'],
            minParameters: 0,
            execute: (message, ...params) => async send => {
                const state = State.getState();
                const leaderboard = state.data.leaderboard;
                const players = state.data.players;

                await executeStats({
                    message,
                    params,
                    send,
                    getPlayer: rankString => {
                        const rank = parseInt(rankString);
                        if (!isNaN(rank) && rank >= 1 && rank <= leaderboard.length) {
                            return leaderboard[rank - 1];
                        }

                        const clean = utils.cleanName(rankString);
                        return players.get(clean.toLowerCase());
                    },
                    getNotFoundMessage: invalid => {
                        const plural = invalid.length === 1 ? 'rank' : 'ranks';
                        return `Invalid ${plural}: ${invalid.join(', ')}`;
                    },
                });
            },
        },
        {
            command: 'leaderboard',
            description: `Returns the top ${config.bot.leaderboardSize} leaderboard for /r/${config.reddit.subreddit}`,
            execute: (message) => async send => {
                await initializeReactableMessage(message.author, 'leaderboard', send);
            },
        },
    ],
};

interface Output {
    truncated: boolean;
    invalid: Set<string>;
    invalidUser: boolean;
    channelId: string;
    players: Set<Bot.Player>;
}

interface GetPlayerArg {
    (arg: string): Bot.Player | undefined;
}

interface ExecuteStatsArgs {
    message: Discord.Message;
    params: string[];
    send: Bot.SendMessageCallback;
    getPlayer: GetPlayerArg;
    getNotFoundMessage: (invalidArray: string[]) => string;
}

async function executeStats({ message, params, send, getPlayer, getNotFoundMessage }: ExecuteStatsArgs) {
    const recent = await checkCooldown(message);
    if (!recent) { return; }

    const channelId = message.channel.id;
    const output: Output = {
        invalid: new Set<string>(),
        invalidUser: false,
        truncated: false,
        channelId,
        players: recent.queries,
    };

    if (params.length === 0) {
        addCommander(output, message);
    }
    params.forEach(param => addIfRoom(output, param, getPlayer));

    const response = buildResponse(output, getNotFoundMessage);
    await sendResponse(message.channel.id, response, send, recent);
}

function addIfRoom(output: Output, param: string, convert: GetPlayerArg) {
    const sanitized = utils.sanitizeWord(param);
    const player = convert(sanitized);

    if (!player) {
        output.invalid.add(sanitized);
    } else if (output.players.size < config.bot.maxStats) {
        output.players.add(player);
    } else if (!output.players.has(player)) {
        output.truncated = true;
    }
}

function addCommander(output: Output, message: Discord.Message) {
    if (message.member) {
        const players = State.getState().data.players;
        const name = utils.cleanName(message.member.nickname ?? message.member.user.username);
        addIfRoom(output, name, n => players.get(n.toLowerCase()));
    } else {
        output.invalidUser = true;
    }
}

function buildResponse(output: Output, getNotFoundMessage: (invalidArray: string[]) => string): string {
    let response = '';

    if (output.invalidUser) {
        response += 'Sorry, I couldn\'t work out whose wins you were looking for.\n\n';
    }

    if (output.invalid.size > 0) {
        const maxInvalid = 5;
        const invalidArray = [...output.invalid].slice(0, maxInvalid).map(p => p.truncate());
        response = getNotFoundMessage(invalidArray);
        if (output.invalid.size > maxInvalid) {
            response += `, and ${output.invalid.size - maxInvalid} others`;
        }
        response += '.\n\n';
    }

    if (output.truncated) {
        response += `Too many players requested, displaying the first ${config.bot.maxStats}:\n\n`;
    }

    if (output.players.size === 1) {
        const player = output.players.values().next().value;
        response +=
            `Here are the stats for **${player.name}**:\n\n` +
            `Rank #**${player.rank}**\n` +
            `Total Wins: **${player.winCount}** :medal:`;
    } else {
        const sortedPlayers = [...output.players].sort((a, b) => a.rank - b.rank);
        response += sortedPlayers.map(utils.formatPlayer).join('\n');
    }

    return response;
}

async function sendResponse(
    channelId: string,
    baseMessage: string,
    send: Bot.SendMessageCallback,
    recent: Bot.RecentCommands<Bot.Player>) {

    const state = State.getState();

    const replyMsg = await send(baseMessage) as Discord.Message;
    recent.message = replyMsg;
    state.data.recentStats.set(channelId, recent);
}

async function checkCooldown(message: Discord.Message): Promise<Bot.RecentCommands<Bot.Player> | undefined> {
    const state = State.getState();
    const channelId = message.channel.id;
    const recentStats = state.data.recentStats.get(channelId);

    if (recentStats) {
        if (recentStats.queries?.size >= config.bot.maxStats) {
            const response = await message.reply(
                `too many stats have been requested recently in this channel.\n` +
                `Visit ${config.api.uiUrl} to see all stats.`);
            await utils.deleteIfRejected(message, response);
            return undefined;
        }
        if (recentStats.message) {
            await recentStats.message.delete();
        }
        clearTimeout(recentStats.timer);
    }

    return {
        message, // this will be overwritten outside of this func
        queries: recentStats ? recentStats.queries : new Set<Bot.Player>(),
        timer: setTimeout(
            () => state.data.recentStats.delete(channelId),
            config.bot.statsCooldown * 1000 * 60),
    };
}
