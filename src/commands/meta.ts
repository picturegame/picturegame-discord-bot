import * as Bot from '../model/bot';
import { initializeReactableMessage } from '../model/reactions';
import { State } from '../model/state';

import * as utils from '../utils';

export const categories = new Map<string, Bot.CommandSpec>();

export function getNoCategoryText() {
    let output = 'What kind of commands would you like help with?\n';

    categories.forEach((spec, key) => {
        output += `\n${key} - ${spec.description}`;
    });

    return output;
}

export const metaCommands: Bot.CommandSpec = {
    description: 'Commands',
    commands: [
        {
            command: 'help',
            description: 'Displays help for various commands',
            execute: (message) => async send => {
                await initializeReactableMessage(message.author, 'help', send);
            },
        },
        {
            command: 'permissions',
            description: 'Checks the required role to use a command',
            parameters: ['command name'],
            execute: (message, rawCommand) => async send => {
                const commandName = utils.sanitizeWord(rawCommand);
                const command = State.getState().getCommand(commandName);
                let response;

                if (command) {
                    response = `Roles that can use command "${commandName}": `;
                    const permissions = command.permissions;
                    response += permissions ? [...permissions].join(', ') : '(any role)';
                } else {
                    response = utils.commandNotFound(commandName, message);
                }

                await send(response);
            },
        },
    ],
};
