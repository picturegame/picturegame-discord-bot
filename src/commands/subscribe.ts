import * as Discord from 'discord.js';
import * as api from 'picturegame-api-wrapper';

import * as Bot from '../model/bot';
import { State } from '../model/state';

import * as Subscriptions from '../reddit/Subscription';

import * as utils from '../utils';

import * as config from '../../config.json';

export const subscribeCommands: Bot.CommandSpec = {
    description: 'Subscriptions & Notifications',
    guideUrl: 'https://gitlab.com/picturegame/picturegame-discord-bot/-/blob/master/doc/subscriptions.md',
    commands: [
        {
            command: 'subscribe',
            description: 'Subscribe to notifications for comments on the given round',
            parameters: ['round number'],
            minParameters: 0,
            execute: (message, ...args) => async send => {
                const mode = args[0]?.toLowerCase();
                if (mode === 'auto') {
                    await handleAutoSubscribe('subscribe', message, send, args[1]);
                } else if (mode === 'host') {
                    await handleSubscribe('subscribe', 'host', message, send, args[1]);
                } else {
                    await handleSubscribe('subscribe', 'players', message, send, args[0]);
                }
            },
        },
        {
            command: 'unsubscribe',
            description: 'Unsubscribe from notifications for comments on the given round',
            parameters: ['round number|all'],
            minParameters: 0,
            execute: (message, ...args) => async send => {
                const firstArg = args[0]?.toLowerCase();
                if (firstArg === 'auto') {
                    await handleAutoSubscribe('unsubscribe', message, send, args[1]);
                } else if (firstArg === 'all') {
                    await Subscriptions.unsubscribeAll(message.author);
                    const successMessage = 'Success. I\'ll no longer DM you for any comments on any thread.';
                    await utils.trySendDM(message.author, successMessage);
                } else if (firstArg === 'host') {
                    await handleSubscribe('unsubscribe', 'host', message, send, args[1]);
                } else {
                    await handleSubscribe('unsubscribe', 'players', message, send, args[0]);
                }
            },
        },
        {
            command: 'notify',
            description: 'Enable notifications for rounds starting and ending',
            parameters: ['round number|player'],
            minParameters: 0,
            execute: (message, ...args) => async send => {
                await handleNotify('add', message, send, ...args);
            },
        },
        {
            command: 'unnotify',
            description: 'Disable notifications for rounds starting and ending',
            parameters: ['round number|player'],
            minParameters: 0,
            execute: (message, ...args) => async send => {
                if (args[0]?.toLowerCase() === 'all') {
                    await Subscriptions.removeAllNotifications(message.author.id);
                    await utils.trySendDM(message.author,
                        'Success. I\'ll no longer DM you for any rounds starting or ending.');
                } else {
                    await handleNotify('remove', message, send, ...args);
                }
            },
        },
    ],
};

async function handleNotify(
    type: 'add' | 'remove',
    message: Discord.Message,
    send: Bot.SendMessageCallback,
    ...args: string[]) {

    const arg = args[0];

    if (arg?.toLowerCase() === 'player') {
        const name = args[1];
        await handleNotifyPlayer(type, message, name, send);
        return;
    }

    const roundNum = arg ? parseInt(arg, 10) : State.getState().data.round.roundNumber!;
    if (isNaN(roundNum)) {
        await handleNotifyPlayer(type, message, arg, send);
    } else {
        await handleNotifyRound(type, message, roundNum, send);
    }
}

async function handleNotifyPlayer(
    type: 'add' | 'remove',
    message: Discord.Message,
    username: string,
    send: Bot.SendMessageCallback) {

    username = utils.sanitizeWord(username);

    if (!username.match(utils.redditUsernamePattern)) {
        await send('Invalid username!');
        return;
    }

    if (type === 'add') {
        await Subscriptions.addNotifyPlayer(username, message.author);
    } else {
        await Subscriptions.removeNotifyPlayer(username, message.author);
    }

    const successMessage = type === 'remove'
        ? `Success. I'll no longer notify you for rounds by ${username}.`
        : `Success. I'll DM you whenever a round by ${username} starts, and again when it ends.`;

    try {
        await message.author.send(successMessage);
    } catch (e) {
        if (type === 'add') {
            // Can't DM this user; revert the subscription and notify in public chat
            await send('I wasn\'t able to send you a DM. Have you blocked me?');
            await Subscriptions.removeNotifyPlayer(username, message.author);
        }
    }
}

async function handleNotifyRound(
    type: 'add' | 'remove',
    message: Discord.Message,
    roundNumber: number,
    send: Bot.SendMessageCallback) {

    const state = State.getState();
    const currentRound = state.data.round;
    const isCurrent = roundNumber === currentRound.roundNumber;

    if (roundNumber < currentRound.roundNumber!
        || (isCurrent && (currentRound.status === 'abandoned' || currentRound.status === 'over'))) {
        await send(`Round ${roundNumber} is already over.`);
        return;
    }

    if (type === 'add') {
        await Subscriptions.addNotifyRound(roundNumber, message.author);
    } else {
        await Subscriptions.removeNotifyRound(roundNumber, message.author);
    }

    const successMessage = type === 'remove'
        ? `Success. I'll no longer notify you for round ${roundNumber}.`
        : isCurrent
            ? `Success. I'll DM you when round ${roundNumber} ends.`
            : `Success. I'll DM you when round ${roundNumber} starts, and again when it ends.`;

    try {
        await message.author.send(successMessage);
    } catch (e) {
        if (type === 'add') {
            // Can't DM this user; revert the subscription and notify in public chat
            await send('I wasn\'t able to send you a DM. Have you blocked me?');
            await Subscriptions.removeNotifyRound(roundNumber, message.author);
        }
    }
}

async function handleSubscribe(
    type: 'subscribe' | 'unsubscribe',
    mode: Subscriptions.CommentSubscribeMode,
    message: Discord.Message,
    send: Bot.SendMessageCallback,
    rawRoundNumber?: string) {

    const state = State.getState();
    const currentRound = state.data.round;

    let round: api.Round | undefined = undefined;
    if (rawRoundNumber) {
        round = await getRound(state, rawRoundNumber);
        if (round === undefined) {
            // Invalid number or API doesn't have the round's id
            await send('Invalid round number!');
            return;
        }
    } else if (currentRound.status !== 'unsolved' && type === 'subscribe') {
        await send('No round is currently in progress.');
        return;
    }

    const nickname = (message.member?.nickname ?? message.member?.user.username)?.toLowerCase();
    if (nickname && mode === 'host') {
        if (round && nickname === round.hostName?.toLowerCase()) {
            await send('You are the host of that round; you cannot subscribe to your own comments!');
            return;
        }
        if (!round && nickname === currentRound.host?.toLowerCase()) {
            await send('You are the host of the current round; you cannot subscribe to your own comments!');
            return;
        }
    }

    if (type === 'subscribe') {
        await Subscriptions.subscribe(mode, message.author, round?.id);
    } else {
        await Subscriptions.unsubscribe(mode, message.author, round?.id);
    }

    let successMessage: string;
    const commentSource = mode === 'host' ? 'the host' : 'players';
    if (round !== undefined) {
        const threadUrl = `<https://redd.it/${round.id}>`;
        successMessage = type === 'subscribe'
            ? `Success. I'll DM you for every new comment from ${commentSource} on this thread: ${threadUrl}`
            : `Success. I'll no longer DM you for comments from ${commentSource} on this thread: ${threadUrl}`;

    } else {
        successMessage = type === 'subscribe'
            ? `Success. I'll DM you for every new comment from ${commentSource} on the current round, until it ends.`
            : `Success. I'll no longer DM you for comments from ${commentSource} on the current round.`;

        if (type === 'subscribe' && currentRound.host && nickname === currentRound.host.toLowerCase()) {
            const messageText = message.content.substring(config.discord.commandPrefix.length);
            const commandText = utils.sanitizeWord(messageText.split(/\s+/)[0]);
            successMessage += `\nDid you know you can do \`!${commandText} auto\` ` +
                'to automatically subscribe to your rounds whenever you host?';
        }
    }

    try {
        await message.author.send(successMessage);
    } catch (e) {
        if (type === 'subscribe') {
            // Can't DM this user; revert the subscription and notify in public chat
            await send('I wasn\'t able to send you a DM. Have you blocked me?');
            await Subscriptions.unsubscribe(mode, message.author, round?.id);
        }
    }
}

async function handleAutoSubscribe(
    type: 'subscribe' | 'unsubscribe',
    message: Discord.Message,
    send: Bot.SendMessageCallback,
    username?: string) {

    const state = State.getState();
    const round = state.data.round;

    if (!username) {
        username = message.member?.nickname ?? message.author.username;
    }

    username = utils.sanitizeWord(username);
    const userLower = username.toLowerCase();

    if (!username.match(utils.redditUsernamePattern)) {
        await send('Invalid username!');
        return;
    }

    if (round.host?.toLowerCase() === userLower && round.status === 'unsolved') {
        type === 'subscribe'
            ? await Subscriptions.subscribe('players', message.author)
            : await Subscriptions.unsubscribe('players', message.author);
    }

    type === 'subscribe'
        ? await Subscriptions.autoSubscribe(message.author, userLower)
        : await Subscriptions.unAutoSubscribe(message.author, userLower);

    const successMessage = type === 'subscribe'
        ? `Success. I'll DM you for every comment on rounds by ${username}, while they're active.`
        : `Success. I'll no longer DM you for comments on rounds by ${username}.`;

    try {
        await message.author.send(successMessage);
    } catch (e) {
        if (type === 'subscribe') {
            // Can't DM this user; revert the subscription and notify in public chat
            await send('I wasn\'t able to send you a DM. Have you blocked me?');
            await Subscriptions.unAutoSubscribe(message.author, userLower);
            if (round.host?.toLowerCase() === userLower && round.status === 'unsolved') {
                await Subscriptions.unsubscribe('players', message.author);
            }
        }
    }
}

/**
 * Get the round id from the given round number, if the round exists and has not been archived
 */
async function getRound(state: State, rawRoundNumber: string): Promise<api.Round | undefined> {

    const roundNumber = parseInt(rawRoundNumber, 10);

    if (isNaN(roundNumber)) {
        return;
    }

    try {
        const round = await state.api.getRound(roundNumber);
        if ((round?.postTime ?? 0) < utils.getArchiveThreshold()) {
            return undefined;
        }
        return round;
    } catch (e) {
        if (e.statusCode === 404 || e.statusCode === 400) {
            return undefined;
        }
        utils.logError(e, 'Error getting round for subscribe/unsubscribe');
        throw e;
    }
}
