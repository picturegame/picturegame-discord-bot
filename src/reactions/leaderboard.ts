import { ReactionHandler } from '../model/bot';
import { State } from '../model/state';

import * as utils from '../utils';

import * as config from '../../config.json';

interface LeaderboardState {
    page: number;
}

export const leaderboardReactionHandler: ReactionHandler<LeaderboardState> = {
    type: 'leaderboard',
    emojis: ['⏮', '◀', '▶', '⏭'],
    init: () => {
        return { page: 0 };
    },
    update: (state, emoji) => {
        const leaderboard = State.getState().data.leaderboard;
        const pageSize = config.bot.leaderboardSize;
        const lastPage = Math.ceil(leaderboard.length / pageSize) - 1;
        let newPage: number = 0;

        switch (emoji.name) {
            case '⏮':
                newPage = 0; break;
            case '◀':
                newPage = Math.max(0, state.page - 1); break;
            case '▶':
                newPage = Math.min(lastPage, state.page + 1); break;
            case '⏭':
                newPage = lastPage; break;
        }

        const willUpdate = newPage !== state.page;
        state.page = newPage;
        return willUpdate;
    },
    getContents: state => {
        const leaderboard = State.getState().data.leaderboard;
        const pageSize = config.bot.leaderboardSize;
        const first = state.page * pageSize;
        return utils.formatPlayerList(leaderboard.slice(first, first + pageSize));
    },
};
