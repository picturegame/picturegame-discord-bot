# picturegame-discord-bot
Fork of btwebb's discord bot for /r/PictureGame

## Dependencies
* nodejs 8 LTS or 10 LTS
* yarn
* an instance of the [PictureGame API](https://gitlab.com/picturegame/picturegame-api) is required for some features

## Installation
* clone the repo: `git clone https://gitlab.com/picturegame/picturegame-discord-bot.git`
* `yarn`
  * ignore any warnings about unmet peer dependencies; these are all optional dependencies of `discord.js`.

## Configuring and running
Config for the bot is stored in JSON format. Save this file as `config.json` at the root `picturegame-discord-bot` directory. The fields are described in `src/model/index.d.ts`.

With the config file in place:
* `yarn build:prod`
* `yarn start`

## Contributing
Please see [here](https://gitlab.com/picturegame/picturegame-discord-bot/issues?label_name[]=accepting+PRs) for the list of open issues that I am accepting pull requests for.
Before submitting a pull request, please ensure that your code passes tslint by running `yarn lint`.

If you have found a bug in the bot, or have a suggestion for an improvement or new feature, please feel free to [submit an issue](https://gitlab.com/picturegame/picturegame-discord-bot/issues/new)
