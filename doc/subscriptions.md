# Subscriptions and Notifications

You can ask the Discord bot to notify you for certain events on and around PictureGame rounds. There are a few things that can be done with this feature; this page will explain each one.

## Aliases

You can use `!sub` instead of `!subscribe`, and `!unsub` instead of `!unsubscribe` in the below examples.

## Subscribe to comments from players

Intended primarily for hosts, with this mode the Bot will send you a DM any time a player (i.e. not the host) comments on a round or rounds of your choice. Will not notify for your own comments, comments from the bot, or comments from the host.

### Subscribe to the current round

`!subscribe`
* This will keep you subscribed until the round ends (either by someone winning it, or it being abandoned), or until you unsubscribe with `!unsubscribe`.

### Subscribe to a specific round

`!subscribe 12345`
* This will keep you subscribed until you unsubscribe with `!unsubscribe 12345`, or until the thread is archived by Reddit.
* Cannot be used for rounds that have not yet been posted.

### Subscribe to all of your own rounds

`!subscribe auto`
* This effectively does `!subscribe` for you automatically, whenever you post a round. These subscriptions also last until the round ends. Disable this subscription with `!unsubscribe auto`.
* Assumes that your Discord nickname matches your Reddit username

### Subscribe to all of a specific user's rounds

`!subscribe auto Provium`
* Works the same as the previous example, but for a specified user instead of yourself. Disable with `!unsubscribe auto Provium`


## Subscribe to comments from the host

Intended for players attempting to solve rounds, with this mode the Bot will send you a DM any time the host comments on a round or rounds of your choice, under the following circumstances:
* Top level (root) comments
* Replies to other comments from the host
* Replies to _you_ (the Reddit user with name matching your Discord nickname)

For obvious reasons, you cannot subscribe to host comments on your own rounds.

### Subscribe to the current round

`!subscribe host`
* This will keep you subscribed until the round ends, or until you unsubscribe with `!unsubscribe host`

### Subscribe to a specific round

`!subscribe host 12345`
* This will keep you subscribed until you unsubscribe with `!unsubscribe host 12345`, or until the thread is archived by Reddit.
* Cannot be used for rounds that have not yet been posted.


## Get notified for rounds starting and ending

You can use this feature to be notified when a certain user posts a round, when a specific round number begins, or when a round ends, for example so you know when you can stop searching for an answer.

### Get notified when the current round ends

`!notify`
* This will send you a single DM when the current round ends. Only lasts for a single round. Cancel with `!unnotify`

### Get notified when a specific round starts and ends

`!notify 12345678`
* This will send you a single DM when the specified round starts, and another when it ends. Cancel with `!unnotify 12345678`.
* Cannot be used for past rounds.

### Get notified when a specific user posts a round

`!notify Provium`
* This will send you a single DM whenever the specified user posts a round, and another when their round ends. Cancel with `!unnotify Provium`
* **Note**: If you are attempting to enable notifications for a user whose name is entirely numerical, you can use `!notify player 99999999999999999989` to tell the bot you want to subscribe to a player, not a round number. The `player` keyword is optional in all other cases. In this case you would cancel with `!unnotify player 99999999999999999989`.

## Cancel all subscriptions and notifications

`!unsubscribe all`
* This will permanently remove all subscriptions that you have previously enabled.

`!unnotify all`
* This will permanently disable all notifications that you have previously enabled.
